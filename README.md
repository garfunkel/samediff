# samediff

Computes and reports a measure of visual difference between any number of video/image files.