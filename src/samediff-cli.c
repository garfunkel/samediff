/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "utils.h"

#include <dirent.h>
#include <getopt.h>
#include <libsamediff.h>
#include <samediff.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

typedef enum {
	INPUT_EXTS_NONE,
	INPUT_EXTS_SUPPORTED,
	INPUT_EXTS_VIDEO,
	INPUT_EXTS_IMAGE,
	INPUT_EXTS_ALL
} input_exts;

static const char *ERROR_TOO_MANY_INPUT_EXTS = "%s: must provide at most one of "
                                               "'-m/--videos-only', '-i/--images-only', or "
                                               "'-a/--all-files'\n";

static video_list_node *video_list_node_alloc(void)
{
	return calloc(1, sizeof(video_list_node));
}

static void video_list_append(video_list_node *list, video_list_node *node)
{
	if (list->next != NULL) {
		node->next = list->next;
	}

	list->next = node;
}

static void video_list_free(video_list_node **list)
{
	if (list == NULL || *list == NULL) {
		return;
	}

	video_list_node *head = *list;
	video_list_node *node = NULL;

	while (head != NULL) {
		node = head;
		head = head->next;

		samediff_video_fingerprint_free(&node->fingerprint);

		free(node->path);
		free(node);
	}

	list = NULL;
}

static int print_report(const video_list_node *list)
{
	if (list == NULL || list->next == NULL) {
		return EXIT_SUCCESS;
	}

	similarity_cache_group *cache = similarity_cache_group_alloc();

	if (cache == NULL) {
		log_no_mem();

		return -EXIT_FAILURE;
	}

	double similarity = 0;

	for (const video_list_node *node_1 = list; node_1 != NULL; node_1 = node_1->next) {
		for (const video_list_node *node_2 = node_1->next; node_2 != NULL; node_2 = node_2->next) {
			if (strcmp(node_1->path, node_2->path) == 0) {
				continue;
			}

			similarity = samediff_video_fingerprint_compare(node_1->fingerprint,
			                                                node_2->fingerprint);

			if (similarity >= 95) {
				if (similarity_cache_group_set(cache, node_1->path, node_2->path) < 0) {
					similarity_cache_free(&cache);

					return EXIT_FAILURE;
				}
			}
		}
	}

	similarity_cache_group *group = cache;

	for (int group_num = 1;; group_num++) {
		if (group == NULL || group->node == NULL) {
			break;
		}

		if (group_num > 1) {
			printf("\n");
		}

		printf("Duplicate group %d:\n", group_num);

		similarity_cache_node *node = group->node;

		for (int match_num = 1;; match_num++) {
			if (node == NULL) {
				break;
			}

			printf("\tMatch %d: %s\n", match_num, node->path);

			node = node->next;
		}

		group = group->next;
	}

	similarity_cache_free(&cache);

	return EXIT_SUCCESS;
}

static int fingerprint(const char *path, video_list_node **list, const samediff_hw_mode hw_mode)
{
	int ret              = 0;
	SamediffVideo *video = samediff_video_alloc();

	if (video == NULL) {
		log_no_mem();

		return -EXIT_FAILURE;
	}

	if ((ret = samediff_video_open(video, path, hw_mode)) != 0) {
		samediff_video_free(&video);

		if (hw_mode == SAMEDIFF_HW_MODE_OFF) {
			log_warning("Cannot open video: %s\n", path);

			return -EXIT_FAILURE;
		}

		return -2;
	}

	video_list_node *node = video_list_node_alloc();

	if (node == NULL) {
		log_no_mem();

		samediff_video_free(&video);

		return -EXIT_FAILURE;
	}

	node->path = strdup(path);

	if (node->path == NULL) {
		log_no_mem();

		samediff_video_free(&video);

		return -EXIT_FAILURE;
	}

	node->fingerprint = samediff_video_fingerprint_alloc();

	if (node->fingerprint == NULL) {
		log_warning("Cannot allocate memory for video fingerprint: %s\n", node->path);

		samediff_video_free(&video);

		free(node->path);
		free(node);

		return -EXIT_FAILURE;
	}

	if (samediff_video_fingerprint_video(video, node->fingerprint, 0) < 0) {
		log_warning("Cannot fingerprint video: %s\n", node->path);

		samediff_video_fingerprint_free(&node->fingerprint);
		samediff_video_free(&video);

		free(node->path);
		free(node);

		return -EXIT_FAILURE;
	}

	samediff_video_free(&video);

	if (*list == NULL) {
		*list = node;
	} else {
		video_list_append(*list, node);
	}

	return EXIT_SUCCESS;
}

static int process_path(const char *path, video_list_node **list, thread_pool *pool);

static int process_dir(const char *path, video_list_node **list, thread_pool *pool)
{
	DIR *dir = opendir(path);

	if (dir == NULL) {
		log_warning("Cannot open directory: %s: %s", path, strerror(errno));

		return -EXIT_FAILURE;
	}

	// Reset errno as readdir() leaves errno unchanged upon EOF.
	errno              = 0;
	struct dirent *ent = NULL;

	while ((ent = readdir(dir)) != NULL) {
		if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
			continue;
		}

		char *joined = join_path(path, ent->d_name);

		if (joined == NULL) {
			log_no_mem();

			break;
		}

		process_path(joined, list, pool);

		free(joined);
	}

	if (errno != 0) {
		log_warning("Cannot read directory: %s", path);

		closedir(dir);

		return EXIT_FAILURE;
	}

	closedir(dir);

	return EXIT_SUCCESS;
}

static int process_path(const char *path, video_list_node **list, thread_pool *pool)
{
	if (path != NULL && strcmp(path, "-") != 0 && strcmp(path, "--") != 0) {
		struct stat info;

		if (stat(path, &info) < 0) {
			log_warning("Cannot stat file: %s: %s", path, strerror(errno));

			return -EXIT_FAILURE;
		}

		if (S_ISDIR(info.st_mode)) {
			return process_dir(path, list, pool);
		} else if (S_ISREG(info.st_mode)) {
			char *path_copy = strdup(path);

			if (path_copy == NULL) {
				log_no_mem();

				return -EXIT_FAILURE;
			}

			if (thread_pool_job(pool, (thread_pool_func)fingerprint, path_copy, list) < 0) {
				log_error("Cannot start fingerprint thread: %s", path_copy);

				free(path_copy);

				return -EXIT_FAILURE;
			}
		} else {
			log_warning("Not a regular file or directory: %s", path);

			return -EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
	struct option options[] = {{"videos-only", no_argument, NULL, 'm'},
	                           {"images-only", no_argument, NULL, 'i'},
	                           {"all-files", no_argument, NULL, 'a'},
	                           {"gpu-threads", required_argument, NULL, 'g'},
	                           {"cpu-threads", required_argument, NULL, 'c'},
	                           {"verbose", no_argument, NULL, 'v'},
	                           {"version", no_argument, NULL, 'V'},
	                           {"help", no_argument, NULL, 'h'},
	                           {0}};

	int opt                 = 0;
	int gpu_threads         = DEFAULT_GPU_DECODING_THREADS;
	int default_cpu_threads = sysconf(_SC_NPROCESSORS_CONF);

	if (default_cpu_threads < 0) {
		default_cpu_threads = FALLBACK_CPU_DECODING_THREADS;
	}

	int cpu_threads = default_cpu_threads;

	input_exts exts = INPUT_EXTS_NONE;

	while ((opt = getopt_long(argc, argv, "miag:c:vVh", options, NULL)) != -1) {
		switch (opt) {
			case 'm':
				if (exts != INPUT_EXTS_NONE) {
					log_error(ERROR_TOO_MANY_INPUT_EXTS, argv[0]);

					return EXIT_FAILURE;
				}

				exts = INPUT_EXTS_VIDEO;

				break;

			case 'i':
				if (exts != INPUT_EXTS_NONE) {
					log_error(ERROR_TOO_MANY_INPUT_EXTS, argv[0]);

					return EXIT_FAILURE;
				}

				exts = INPUT_EXTS_IMAGE;

				break;

			case 'a':
				if (exts != INPUT_EXTS_NONE) {
					log_error(ERROR_TOO_MANY_INPUT_EXTS, argv[0]);

					return EXIT_FAILURE;
				}

				exts = INPUT_EXTS_SUPPORTED;

				break;

			case 'g':
				gpu_threads = atoi(optarg);

				break;

			case 'c':
				cpu_threads = atoi(optarg);

				break;

			case 'v':
				set_log_level(LOG_LEVEL_INFO);

				break;

			case 'V':
				printf("samediff %s\n", VERSION);

				return EXIT_SUCCESS;

			case 'h':
				printf("Usage: %s [OPTION]... FILE...\n"
				       "Computes and reports a measure of visual difference between any number of "
				       "video/image files.\n"
				       "FILE arguments may be regular files, symlinks, or directories.\n\n"
				       "Options:\n"
				       "  -m, --videos-only  operate on supported video file extensions only\n"
				       "  -i, --images-only  operate on supported image file extensions only\n"
				       "  -a, --all-files    operate on all files regardless of extension\n"
				       "  -g, --gpu-threads  number of GPU decoding threads (default: %d)\n"
				       "  -c, --cpu-threads  number of CPU decoding threads (default: %d)\n"
				       "  -v  --verbose      increase verbosity\n"
				       "  -V  --version      print version (%s) and exit\n"
				       "  -h  --help         print this help and exit\n",
				       argv[0],
				       DEFAULT_GPU_DECODING_THREADS,
				       default_cpu_threads,
				       VERSION);

				return EXIT_SUCCESS;

			// '?'
			default:
				return EXIT_FAILURE;
		}

		// Remove option from argv.
		argv[optind - 1] = NULL;
	}

	if (gpu_threads < 0) {
		log_error("Invalid number of GPU threads: %d", gpu_threads);

		return EXIT_FAILURE;
	}

	if (cpu_threads < 1) {
		log_error("Invalid number of CPU threads: %d", cpu_threads);

		return EXIT_FAILURE;
	}

	if (exts == INPUT_EXTS_NONE) {
		exts = INPUT_EXTS_SUPPORTED;
	}

	video_list_node *list = NULL;
	thread_pool *pool     = thread_pool_init(gpu_threads, cpu_threads);

	if (pool == NULL) {
		log_no_mem();

		return EXIT_FAILURE;
	}

	for (int i = 1; i < argc; i++) {
		process_path(argv[i], &list, pool);
	}

	thread_pool_wait(pool);
	thread_pool_free(&pool);

	print_report(list);

	video_list_free(&list);

	return EXIT_SUCCESS;
}
