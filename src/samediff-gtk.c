/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include "libsamediff.h"
#include "samediff-gui.h"
#include "samediff.h"
#include "utils.h"

#include <gtk/gtk.h>
#include <stdbool.h>
#include <threads.h>

static void setup_file_list_model(GtkTreeView *tree_view_files)
{
	GtkListStore *list_store_files = gtk_list_store_new(7,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_STRING,
	                                                    G_TYPE_FLOAT);

	gtk_tree_view_set_model(tree_view_files, GTK_TREE_MODEL(list_store_files));
	gtk_tree_view_set_headers_visible(tree_view_files, true);

	gtk_widget_set_hexpand(GTK_WIDGET(tree_view_files), true);

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *name_column =
	    gtk_tree_view_column_new_with_attributes("Name", renderer, "text", 0, NULL);

	gtk_tree_view_column_set_expand(name_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), name_column);

	GtkTreeViewColumn *fingerprint_column =
	    gtk_tree_view_column_new_with_attributes("Fingerprint", renderer, "text", 1, NULL);

	gtk_tree_view_column_set_expand(fingerprint_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), fingerprint_column);

	GtkTreeViewColumn *format_column =
	    gtk_tree_view_column_new_with_attributes("Format", renderer, "text", 2, NULL);

	gtk_tree_view_column_set_expand(format_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), format_column);

	GtkTreeViewColumn *codec_column =
	    gtk_tree_view_column_new_with_attributes("Codec", renderer, "text", 3, NULL);

	gtk_tree_view_column_set_expand(codec_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), codec_column);

	GtkTreeViewColumn *size_column =
	    gtk_tree_view_column_new_with_attributes("Size", renderer, "text", 4, NULL);

	gtk_tree_view_column_set_expand(size_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), size_column);

	GtkTreeViewColumn *resolution_column =
	    gtk_tree_view_column_new_with_attributes("Resolution", renderer, "text", 5, NULL);

	gtk_tree_view_column_set_expand(resolution_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), resolution_column);

	GtkTreeViewColumn *fps_column =
	    gtk_tree_view_column_new_with_attributes("Framerate", renderer, "text", 6, NULL);

	gtk_tree_view_column_set_expand(fps_column, true);

	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_files), fps_column);
}

typedef struct {
	gchar *path;
	GFileInfo *info;
	GtkTreeView *view;
} fingerprint_thread_data;

typedef struct {
	GtkTreeView *view;
	SamediffVideoInfo *video_info;
	char *path;
	char *resolution;
	char *size;
} video_list_add_data;

static gboolean video_list_add(gpointer user_data)
{
	video_list_add_data *list_data = (video_list_add_data *)user_data;

	GtkListStore *store = GTK_LIST_STORE(gtk_tree_view_get_model(list_data->view));

	if (store == NULL) {
		return -ENOMEM;
	}

	GtkTreeIter iter;

	gtk_list_store_append(store, &iter);

	gtk_list_store_set(store,
	                   &iter,
	                   0,
	                   list_data->path,
	                   1,
	                   "Queued",
	                   2,
	                   list_data->video_info->format,
	                   3,
	                   list_data->video_info->codec,
	                   4,
	                   list_data->size,
	                   5,
	                   list_data->resolution,
	                   6,
	                   list_data->video_info->fps,
	                   -1);

	return true;
}

static int fingerprint_file(void *arg)
{
	fingerprint_thread_data *thread_data = (fingerprint_thread_data *)arg;
	SamediffVideo *video                 = samediff_video_alloc();

	if (video == NULL) {
		return -ENOMEM;
	}

	int ret = samediff_video_open(video, thread_data->path, SAMEDIFF_HW_MODE_ATTEMPT);

	if (ret < 0) {
		return ret;
	}

	SamediffVideoInfo *video_info = samediff_video_get_info(video);

	char *resolution = NULL;
	char *size       = human_readable_size(g_file_info_get_size(thread_data->info));

	asprintf(&resolution, "%dx%d", video_info->width, video_info->height);

	video_list_add_data *list_data = g_slice_alloc(sizeof(video_list_add_data));

	list_data->view       = thread_data->view;
	list_data->video_info = video_info;
	list_data->path       = (char *)g_file_info_get_name(thread_data->info);
	list_data->resolution = resolution;
	list_data->size       = size;

	g_idle_add(video_list_add, list_data);

	// free(size);
	// free(resolution);
	// free(thread_data->path);
	// g_object_unref(thread_data->info);
	// free(thread_data);

	samediff_video_free(&video);

	return ret;
}

static void add_file(const gchar *path, GtkTreeView *view)
{
	GFile *file = g_file_new_for_path(path);

	if (file == NULL) {
		return;
	}

	GFileInfo *info = g_file_query_info(file, "standard::*", G_FILE_QUERY_INFO_NONE, NULL, NULL);

	if (info == NULL) {
		return;
	}

	if (g_file_info_get_file_type(info) == G_FILE_TYPE_DIRECTORY) {
		GFileEnumerator *iter =
		    g_file_enumerate_children(file, "standard::*", G_FILE_QUERY_INFO_NONE, NULL, NULL);

		if (iter == NULL) {
			exit(EXIT_FAILURE);
		}

		while (true) {
			GFileInfo *sub_info = g_file_enumerator_next_file(iter, NULL, NULL);

			if (sub_info == NULL) {
				break;
			}

			GFile *sub_file = g_file_enumerator_get_child(iter, sub_info);

			if (sub_file == NULL) {
				break;
			}

			gchar *sub_path = g_file_get_path(sub_file);

			add_file(sub_path, view);

			g_free(sub_path);
			g_object_unref(sub_file);
			g_object_unref(sub_info);
		}

		g_object_unref(iter);
	} else {
		thrd_t thread;
		fingerprint_thread_data *data = malloc(sizeof(fingerprint_thread_data));

		data->path = strdup(path);
		data->info = g_file_info_dup(info);
		data->view = view;

		thrd_create(&thread, fingerprint_file, data);
		thrd_detach(thread);
	}

	g_object_unref(info);
	g_object_unref(file);
}

static void add_files(const GSList *list, GtkTreeView *view)
{
	GSList *node = (GSList *)list;

	while (node != NULL) {
		add_file((const gchar *)node->data, view);

		node = node->next;
	}
}

static void on_add_videos_clicked(GtkToolButton *button, GtkTreeView *view)
{
	GtkApplicationWindow *window =
	    GTK_APPLICATION_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(button)));

	if (window == NULL) {
		exit(EXIT_FAILURE);
	}

	GtkFileChooserDialog *dialog =
	    GTK_FILE_CHOOSER_DIALOG(gtk_file_chooser_dialog_new("Select Videos",
	                                                        GTK_WINDOW(window),
	                                                        GTK_FILE_CHOOSER_ACTION_OPEN,
	                                                        "_Cancel",
	                                                        GTK_RESPONSE_CANCEL,
	                                                        "_Select",
	                                                        GTK_RESPONSE_ACCEPT,
	                                                        NULL));

	if (dialog == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), true);

	GtkFileFilter *filter_videos = gtk_file_filter_new();

	if (filter_videos == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_file_filter_set_name(filter_videos, "Video files");

	for (int i = 0;; i++) {
		const char *ext = VIDEO_EXTENSIONS[i];

		if (ext == NULL) {
			break;
		}

		char *pattern = malloc(strlen(ext) + 3);

		strcat(strcpy(pattern, "*."), ext);

		gtk_file_filter_add_pattern(filter_videos, pattern);

		free(pattern);
	}

	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter_videos);

	GtkFileFilter *filter_images = gtk_file_filter_new();

	if (filter_images == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_file_filter_set_name(filter_images, "Image files");

	for (int i = 0;; i++) {
		const char *ext = IMAGE_EXTENSIONS[i];

		if (ext == NULL) {
			break;
		}

		char *pattern = malloc(strlen(ext) + 3);

		strcat(strcpy(pattern, "*."), ext);

		gtk_file_filter_add_pattern(filter_images, pattern);

		free(pattern);
	}

	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter_images);

	GtkFileFilter *filter_all = gtk_file_filter_new();

	if (filter_all == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_file_filter_set_name(filter_all, "All files");
	gtk_file_filter_add_pattern(filter_all, "*");

	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter_all);

	gint ret = gtk_dialog_run(GTK_DIALOG(dialog));

	if (ret == GTK_RESPONSE_ACCEPT) {
		GSList *file_list = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));

		add_files(file_list, view);

		g_slist_free_full(file_list, g_free);
	}

	gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void on_add_folders_clicked(GtkToolButton *button, GtkTreeView *view)
{
	GtkApplicationWindow *window =
	    GTK_APPLICATION_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(button)));

	GtkFileChooserDialog *dialog =
	    GTK_FILE_CHOOSER_DIALOG(gtk_file_chooser_dialog_new("Select Folders",
	                                                        GTK_WINDOW(window),
	                                                        GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
	                                                        "_Cancel",
	                                                        GTK_RESPONSE_CANCEL,
	                                                        "_Select",
	                                                        GTK_RESPONSE_ACCEPT,
	                                                        NULL));

	if (dialog == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), true);

	gint ret = gtk_dialog_run(GTK_DIALOG(dialog));

	if (ret == GTK_RESPONSE_ACCEPT) {
		GSList *folder_list = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));

		add_files(folder_list, view);

		g_slist_free_full(folder_list, g_free);
	}

	gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void on_remove_clicked(GtkToolButton *button, gpointer user_data) {}

static void on_clear_clicked(GtkToolButton *button, gpointer user_data) {}

static void setup_ui(GtkApplicationWindow *window)
{
	gtk_window_set_title(GTK_WINDOW(window), "Samediff");
	gtk_window_set_default_size(GTK_WINDOW(window), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

	GtkGrid *grid = GTK_GRID(gtk_grid_new());

	if (grid == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(grid));

	GtkToolbar *toolbar = GTK_TOOLBAR(gtk_toolbar_new());

	if (toolbar == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_toolbar_set_style(toolbar, GTK_TOOLBAR_BOTH_HORIZ);

	GtkToolButton *toolbar_button_add_videos = GTK_TOOL_BUTTON(gtk_tool_button_new(
	    gtk_image_new_from_icon_name("video-x-generic", GTK_ICON_SIZE_BUTTON), "Add Videos"));

	if (toolbar_button_add_videos == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_tool_item_set_is_important(GTK_TOOL_ITEM(toolbar_button_add_videos), true);

	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(toolbar_button_add_videos), 0);

	GtkToolButton *toolbar_button_add_folders = GTK_TOOL_BUTTON(gtk_tool_button_new(
	    gtk_image_new_from_icon_name("folder-open", GTK_ICON_SIZE_BUTTON), "Add Folders"));

	if (toolbar_button_add_folders == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_tool_item_set_is_important(GTK_TOOL_ITEM(toolbar_button_add_folders), true);

	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(toolbar_button_add_folders), 1);

	GtkToolButton *toolbar_button_remove = GTK_TOOL_BUTTON(gtk_tool_button_new(
	    gtk_image_new_from_icon_name("list-remove", GTK_ICON_SIZE_BUTTON), "Remove"));

	if (toolbar_button_remove == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_tool_item_set_is_important(GTK_TOOL_ITEM(toolbar_button_remove), true);

	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(toolbar_button_remove), 2);

	GtkToolButton *toolbar_button_clear = GTK_TOOL_BUTTON(gtk_tool_button_new(
	    gtk_image_new_from_icon_name("edit-clear-all", GTK_ICON_SIZE_BUTTON), "Clear"));

	if (toolbar_button_clear == NULL) {
		exit(EXIT_FAILURE);
	}

	gtk_tool_item_set_is_important(GTK_TOOL_ITEM(toolbar_button_clear), true);

	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(toolbar_button_clear), 3);

	gtk_grid_attach(grid, GTK_WIDGET(toolbar), 0, 0, 1, 1);

	GtkTreeView *tree_view_files = GTK_TREE_VIEW(gtk_tree_view_new());

	setup_file_list_model(tree_view_files);

	gtk_grid_attach(grid, GTK_WIDGET(tree_view_files), 0, 1, 1, 1);

	g_signal_connect(
	    toolbar_button_add_folders, "clicked", G_CALLBACK(on_add_folders_clicked), tree_view_files);
	g_signal_connect(
	    toolbar_button_add_videos, "clicked", G_CALLBACK(on_add_videos_clicked), tree_view_files);
	g_signal_connect(toolbar_button_remove, "clicked", G_CALLBACK(on_remove_clicked), NULL);
	g_signal_connect(toolbar_button_clear, "clicked", G_CALLBACK(on_clear_clicked), NULL);
}

static void on_activate(GtkApplication *app, gpointer user_data)
{
	GtkApplicationWindow *window = GTK_APPLICATION_WINDOW(gtk_application_window_new(app));

	if (window == NULL) {
		exit(EXIT_FAILURE);
	}

	setup_ui(window);

	gtk_widget_show_all(GTK_WIDGET(window));
}

int samediff_run(int argc, char *argv[])
{
	samediff_init();

	GtkApplication *app = gtk_application_new("org.simonallen.samediff", G_APPLICATION_FLAGS_NONE);

	if (app == NULL) {
		return EXIT_FAILURE;
	}

	g_signal_connect(app, "activate", G_CALLBACK(on_activate), NULL);

	int ret = g_application_run(G_APPLICATION(app), argc, argv);

	g_object_unref(app);

	return ret;
}
