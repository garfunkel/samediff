/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include "utils.h"

#ifdef __APPLE__
#	include <dispatch/dispatch.h>
#else
#	include <semaphore.h>
#endif
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#	define PATH_SEPARATOR '\\'
#else
#	define PATH_SEPARATOR '/'
#endif

#ifdef __APPLE__
#	include <pthread.h>

#	define thrd_t pthread_t
#	define thrd_start_t void *(*)(void *)
#	define thrd_create(thread, func, arg) pthread_create(thread, NULL, func, arg)
#	define thrd_detach(thread) pthread_detach(thread)
#	define sem_t dispatch_semaphore_t
#	define sem_post(semaphore) dispatch_semaphore_signal(*semaphore)
#	define sem_wait(semaphore) dispatch_semaphore_wait(*semaphore, DISPATCH_TIME_FOREVER)
#	define sem_trywait(semaphore) dispatch_semaphore_wait(*semaphore, DISPATCH_TIME_NOW)
#	define sem_destroy(semaphore)
#else
#	include <threads.h>
#endif

static log_level LOG_LEVEL = LOG_LEVEL_ERROR;

void set_log_level(const log_level level)
{
	LOG_LEVEL = level;
}

log_level get_log_level(void)
{
	return LOG_LEVEL;
}

void log_debug(const char *format, ...)
{
	if (LOG_LEVEL > LOG_LEVEL_DEBUG) {
		return;
	}

	va_list args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	fprintf(stderr, "\n");
}

void log_info(const char *format, ...)
{
	if (LOG_LEVEL > LOG_LEVEL_INFO) {
		return;
	}

	va_list args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	fprintf(stderr, "\n");
}

void log_warning(const char *format, ...)
{
	if (LOG_LEVEL > LOG_LEVEL_WARNING) {
		return;
	}

	va_list args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	fprintf(stderr, "\n");
}

void log_error(const char *format, ...)
{
	va_list args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	fprintf(stderr, "\n");
}

char *human_readable_size(const long bytes)
{
	double size      = bytes;
	int suffix_index = 0;
	char *str        = NULL;
	char *suffixes[] = {"bytes", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", NULL};
	int ret          = 0;

	for (; size >= 1000.0 && suffixes[suffix_index] != NULL; suffix_index++) {
		size /= 1000.0;
	}

	if (suffix_index == 0 || suffixes[suffix_index] == NULL) {
		ret = asprintf(&str, "%ld %s", bytes, suffixes[0]);
	} else {
		ret = asprintf(&str, "%0.2f %s", size, suffixes[suffix_index]);
	}

	if (ret == -1) {
		return NULL;
	}

	return str;
}

char *join_path(const char *path_1, const char *path_2)
{
	char *path_1_stripped = clean_path(path_1, true);

	if (path_1_stripped == NULL) {
		return NULL;
	}

	char *path_2_stripped = clean_path(path_2, false);

	if (path_2_stripped == NULL) {
		return NULL;
	}

	char *joined = malloc(strlen(path_1_stripped) + strlen(path_2_stripped) + 2);

	if (joined == NULL) {
		return NULL;
	}

	strcpy(joined, path_1_stripped);
	strcat(joined, "/");
	strcat(joined, path_2_stripped);

	free(path_1_stripped);
	free(path_2_stripped);

	return joined;
}

char *clean_path(const char *str, const bool preserve_leading_slash)
{
	int len   = strlen(str);
	int start = 0;
	int end   = strlen(str) - 1;

	while (start < len && str[start] == PATH_SEPARATOR) {
		start++;
	}

	// Preserve a single leading /.
	if (preserve_leading_slash && start > 0) {
		start--;
	}

	while (end >= 0 && str[end] == PATH_SEPARATOR) {
		end--;
	}

	return strndup(str + start, end - start + 1);
}

typedef struct thread_pool {
	int cpu_threads;
	int gpu_threads;
	sem_t cpu_semaphore;
	sem_t gpu_semaphore;
} thread_pool;

typedef struct thread_pool_runner_arg {
	thread_pool *pool;
	thread_pool_func func;
	char *path;
	video_list_node **list;
	sem_t *cpu_semaphore;
	sem_t *gpu_semaphore;
	samediff_hw_mode hw_mode;
} thread_pool_runner_arg;

static int thread_pool_runner(thread_pool_runner_arg *arg)
{
	int ret = arg->func(arg->path, arg->list, arg->hw_mode);

	if (arg->hw_mode == SAMEDIFF_HW_MODE_FORCE) {
		if (ret == -2) {
			// Hardware decode failed, try software.
			ret = 0;

			if (sem_wait(arg->cpu_semaphore) != 0) {
				free(arg->path);
				free(arg);

				sem_post(arg->gpu_semaphore);

				return -EXIT_FAILURE;
			}

			sem_post(arg->gpu_semaphore);

			ret = arg->func(arg->path, arg->list, SAMEDIFF_HW_MODE_OFF);

			free(arg->path);
			free(arg);

			sem_post(arg->cpu_semaphore);
		} else {
			free(arg->path);
			free(arg);

			sem_post(arg->gpu_semaphore);
		}
	} else {
		free(arg->path);
		free(arg);

		sem_post(arg->cpu_semaphore);
	}

	return ret;
}

thread_pool *thread_pool_init(const int gpu_threads, const int cpu_threads)
{
	thread_pool *pool = malloc(sizeof(thread_pool));

	if (pool == NULL) {
		return NULL;
	}

	pool->gpu_threads = gpu_threads;
	pool->cpu_threads = cpu_threads;

#ifdef __APPLE__
	if ((pool->gpu_semaphore = dispatch_semaphore_create(gpu_threads)) == NULL) {
#else
	if (sem_init(&pool->gpu_semaphore, 0, gpu_threads) < 0) {
#endif
		free(pool);

		return NULL;
	}

#ifdef __APPLE__
	if ((pool->cpu_semaphore = dispatch_semaphore_create(cpu_threads)) == NULL) {
#else
	if (sem_init(&pool->cpu_semaphore, 0, cpu_threads) < 0) {
#endif
		sem_destroy(&pool->gpu_semaphore);

		free(pool);

		return NULL;
	}

	return pool;
}

// TODO: only HW enable videos, not images.
int thread_pool_job(thread_pool *pool, thread_pool_func func, char *path, video_list_node **list)
{
	if (func == NULL) {
		return EXIT_SUCCESS;
	}

	thread_pool_runner_arg *runner_arg = malloc(sizeof(thread_pool_runner_arg));

	if (runner_arg == NULL) {
		log_no_mem();

		return -EXIT_FAILURE;
	}

	runner_arg->pool          = pool;
	runner_arg->func          = func;
	runner_arg->path          = path;
	runner_arg->list          = list;
	runner_arg->gpu_semaphore = &pool->gpu_semaphore;
	runner_arg->cpu_semaphore = &pool->cpu_semaphore;
	runner_arg->hw_mode       = SAMEDIFF_HW_MODE_FORCE;

	if (sem_trywait(&pool->gpu_semaphore) != 0) {
		if (sem_wait(&pool->cpu_semaphore) != 0) {
			return -EXIT_FAILURE;
		}

		runner_arg->hw_mode = SAMEDIFF_HW_MODE_OFF;
	}

	thrd_t thread;

	thrd_create(&thread, (thrd_start_t)thread_pool_runner, runner_arg);
	thrd_detach(thread);

	return EXIT_SUCCESS;
}

int thread_pool_wait(thread_pool *pool)
{
	for (int i = 0; i < pool->gpu_threads; i++) {
		if (sem_wait(&pool->gpu_semaphore) < 0) {
			return -EXIT_FAILURE;
		}
	}

	for (int i = 0; i < pool->cpu_threads; i++) {
		if (sem_wait(&pool->cpu_semaphore) != 0) {
			return -EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

void thread_pool_free(thread_pool **pool)
{
	sem_destroy(&(*pool)->cpu_semaphore);
	sem_destroy(&(*pool)->gpu_semaphore);

	free(*pool);

	pool = NULL;
}

similarity_cache_group *similarity_cache_group_alloc(void)
{
	return calloc(1, sizeof(similarity_cache_group));
}

static int similarity_cache_group_add(similarity_cache_group *group,
                                      const char *path,
                                      const int distance)
{
	similarity_cache_node *node = group->node;

	while (node != NULL) {
		if (strcmp(node->path, path) == 0) {
			return EXIT_SUCCESS;
		}

		node = node->next;
	}

	node = malloc(sizeof(similarity_cache_node));

	if (node == NULL) {
		log_no_mem();

		return -EXIT_FAILURE;
	}

	node->path     = (char *)path;
	node->distance = distance;
	node->next     = NULL;

	if (group->node != NULL) {
		node->next = group->node;
	}

	group->node = node;

	return EXIT_SUCCESS;
}

int similarity_cache_group_set(similarity_cache_group *group,
                               const char *path_1,
                               const char *path_2)
{
	while (group != NULL) {
		similarity_cache_node *file_1 = NULL;
		similarity_cache_node *file_2 = NULL;
		similarity_cache_node *node   = group->node;

		while (node != NULL) {
			if (node->distance < 2 && strcmp(node->path, path_1) == 0) {
				file_1 = node;
			} else if (node->distance < 2 && strcmp(node->path, path_2) == 0) {
				file_2 = node;
			}

			node = node->next;
		}

		if (file_1 != NULL && file_2 != NULL) {
			return EXIT_SUCCESS;
		} else if (file_1 != NULL) {
			return similarity_cache_group_add(group, path_2, 2);
		} else if (file_2 != NULL) {
			return similarity_cache_group_add(group, path_1, 2);
		}

		if (group->next == NULL) {
			if (group->node != NULL) {
				group->next = similarity_cache_group_alloc();
				group       = group->next;
			}

			if (similarity_cache_group_add(group, path_1, 1) < 0) {
				return -EXIT_FAILURE;
			}

			return similarity_cache_group_add(group, path_2, 1);
		}

		group = group->next;
	}

	return EXIT_SUCCESS;
}

void similarity_cache_free(similarity_cache_group **cache)
{
	if (cache == NULL || *cache == NULL) {
		return;
	}

	similarity_cache_group *group_head = *cache;
	similarity_cache_group *group_node = NULL;

	while (group_head != NULL) {
		group_node = group_head;
		group_head = group_head->next;

		similarity_cache_node *item_head = group_node->node;
		similarity_cache_node *item_node = NULL;

		while (item_head != NULL) {
			item_node = item_head;
			item_head = item_head->next;

			free(item_node);
		}

		free(group_node);
	}

	cache = NULL;
}
