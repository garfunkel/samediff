/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "libsamediff.h"

#include <stdbool.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

/**
 * Default number of GPU decoding threads. CPU threads are determined at runtime by the number of
 * configured CPU cores available - however a fallback value is defined here.
 */
#define DEFAULT_GPU_DECODING_THREADS 0
#define FALLBACK_CPU_DECODING_THREADS 1

/**
 * Logging types/functions.
 */
typedef enum { LOG_LEVEL_DEBUG, LOG_LEVEL_INFO, LOG_LEVEL_WARNING, LOG_LEVEL_ERROR } log_level;

void set_log_level(const log_level level);
log_level get_log_level(void);

void log_debug(const char *format, ...);
void log_info(const char *format, ...);
void log_warning(const char *format, ...);
void log_error(const char *format, ...);

#define log_no_mem() log_error("Cannot allocate memory: %s", strerror(errno))

/**
 * General purpose functions.
 */
char *human_readable_size(const long bytes);
char *join_path(const char *path_1, const char *path_2);
char *clean_path(const char *str, const bool preserve_leading_slash);

typedef struct video_list_node {
	char *path;
	SamediffVideoFingerprint *fingerprint;
	struct video_list_node *next;
} video_list_node;

/**
 * Types and functions for thread pools.
 */
typedef struct thread_pool thread_pool;
typedef int (*thread_pool_func)(const char *path, video_list_node **list, samediff_hw_mode hw_mode);

thread_pool *thread_pool_init(const int gpu_threads, const int cpu_threads);
int thread_pool_job(thread_pool *pool, thread_pool_func func, char *path, video_list_node **list);
int thread_pool_wait(thread_pool *pool);
void thread_pool_free(thread_pool **pool);

/**
 * Types and functions implementing a cache and grouping of similarity comparisons.
 */
typedef struct similarity_cache_node {
	char *path;
	int distance;
	struct similarity_cache_node *next;
} similarity_cache_node;

typedef struct similarity_cache_group {
	similarity_cache_node *node;
	struct similarity_cache_group *next;
} similarity_cache_group;

similarity_cache_group *similarity_cache_group_alloc(void);
int similarity_cache_group_set(similarity_cache_group *group,
                               const char *path_1,
                               const char *path_2);
void similarity_cache_free(similarity_cache_group **group);

__END_DECLS
