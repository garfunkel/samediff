/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "libsamediff.h"

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/hwcontext.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

#define FRAME_FINGERPRINT_WIDTH 8
#define FRAME_FINGERPRINT_HEIGHT 8
#define FRAME_FINGERPRINT_BUFFER_SIZE_BITS FRAME_FINGERPRINT_WIDTH *FRAME_FINGERPRINT_HEIGHT
#define FRAME_FINGERPRINT_BUFFER_SIZE_BYTES FRAME_FINGERPRINT_BUFFER_SIZE_BITS / 8

#define freep(p) \
	free(*p); \
	*p = NULL

#ifdef __clang__
#	pragma GCC diagnostic push
#	pragma GCC diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#endif

#define debug(format, ...) \
	log_message(EXIT_SUCCESS, 0, "DEBUG", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)
#define info(format, ...) \
	log_message(EXIT_SUCCESS, 0, "INFO", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)
#define warning_errno(format, ...) \
	log_message(EXIT_SUCCESS, errno, "WARNING", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)
#define warning(format, ...) \
	log_message(EXIT_SUCCESS, 0, "WARNING", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)
#define error_errno(format, ...) \
	log_message(-EXIT_FAILURE, errno, "ERROR", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)
#define error(format, ...) \
	log_message(-EXIT_FAILURE, 0, "ERROR", __FILE__, __LINE__, __func__, format, ##__VA_ARGS__)

#ifdef __clang__
#	pragma GCC diagnostic pop
#endif

typedef enum { MEDIA_TYPE_VIDEO, MEDIA_TYPE_IMAGE, MEDIA_TYPE_AUDIO } media_type;

struct SamediffVideo {
	AVCodecContext *codec_context;
	AVFormatContext *format_context;
	int stream_index;
	media_type type;
	enum AVPixelFormat hw_pix_format;
	AVBufferRef *hw_device_context;
	enum AVHWDeviceType hw_device;
	AVFrame *grey_frame;
	struct SwsContext *sws_context;
	SamediffVideoInfo *info;
};

struct SamediffVideoFrame {
	AVFrame *frame;
	AVFrame *sw_frame;
	AVFrame *frame_to_fingerprint;
};

struct SamediffVideoFingerprint {
	uint8_t fingerprint[FRAME_FINGERPRINT_BUFFER_SIZE_BYTES];
	SamediffVideoFingerprint *next;
};

static void log_message(const int status,
                        const int errnum,
                        const char *const type,
                        const char *const file,
                        const int line,
                        const char *const function,
                        const char *const format,
                        ...)
{
	va_list args;

	if (file != NULL) {
		fprintf(stderr, "%s:", file);
	}

	if (line > 0) {
		fprintf(stderr, "%d:", line);
	}

	if (function != NULL) {
		fprintf(stderr, "%s(): ", function);
	} else if (file != NULL || line > 0) {
		fprintf(stderr, " ");
	}

	fprintf(stderr, "%s: ", type);

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	if (errnum != 0) {
		fprintf(stderr, ": %s", strerror(errnum));
	}

	fprintf(stderr, "\n");

	if (status != 0) {
		exit(status);
	}
}

static int compute_frame_fingerprint(SamediffVideo *video,
                                     const SamediffVideoFrame *const frame,
                                     const size_t width,
                                     const size_t height,
                                     uint8_t *const fingerprint)
{
	int ret                  = 0;
	int byte                 = 0;
	int bit                  = 0;
	uint8_t pixel            = 0;
	uint8_t comparison_pixel = 0;

	if (video->sws_context == NULL) {
		video->sws_context =
		    sws_getCachedContext(NULL,
		                         frame->frame_to_fingerprint->width,
		                         frame->frame_to_fingerprint->height,
		                         (enum AVPixelFormat)frame->frame_to_fingerprint->format,
		                         width,
		                         height,
		                         AV_PIX_FMT_GRAY8,
		                         0,
		                         NULL,
		                         NULL,
		                         NULL);

		if (video->sws_context == NULL) {
			return AVERROR(ENOMEM);
		}
	}

	if (video->grey_frame == NULL) {
		video->grey_frame = av_frame_alloc();

		if (video->grey_frame == NULL) {
			return AVERROR(ENOMEM);
		}

		video->grey_frame->width  = width;
		video->grey_frame->height = height;

		if ((ret = av_image_alloc(video->grey_frame->data,
		                          video->grey_frame->linesize,
		                          width,
		                          height,
		                          AV_PIX_FMT_GRAY8,
		                          16)) < 0) {
			return ret;
		}
	}

	if ((ret = sws_scale(video->sws_context,
	                     (const uint8_t *const *)frame->frame_to_fingerprint->data,
	                     frame->frame_to_fingerprint->linesize,
	                     0,
	                     frame->frame_to_fingerprint->height,
	                     video->grey_frame->data,
	                     video->grey_frame->linesize)) < 0) {
		return ret;
	}

	for (int pixel_index = 0; pixel_index < video->grey_frame->width * video->grey_frame->height;
	     pixel_index++) {
		pixel = *(video->grey_frame->data[0] + pixel_index);

		if (pixel_index == video->grey_frame->width * video->grey_frame->height - 1) {
			comparison_pixel = *(video->grey_frame->data[0]);
		} else {
			comparison_pixel = *(video->grey_frame->data[0] + pixel_index + 1);
		}

		if (pixel > comparison_pixel) {
			fingerprint[byte] |= (1 << (7 - bit));
		}

		if (bit == 7) {
			byte++;
			bit = 0;
		} else {
			bit++;
		}
	}

	return 0;
}

SamediffVideoFrame *samediff_video_frame_alloc(void)
{
	SamediffVideoFrame *frame = calloc(1, sizeof(SamediffVideoFrame));

	if ((frame->frame = av_frame_alloc()) == NULL) {
		samediff_video_frame_free(&frame);
	}

	if ((frame->sw_frame = av_frame_alloc()) == NULL) {
		samediff_video_frame_free(&frame);
	}

	return frame;
}

void samediff_video_frame_free(SamediffVideoFrame **frame)
{
	if (frame == NULL || *frame == NULL) {
		return;
	}

	av_frame_free(&((*frame)->sw_frame));
	av_frame_free(&((*frame)->frame));

	(*frame)->frame_to_fingerprint = NULL;

	freep(frame);
}

int samediff_init(void)
{
	av_log_set_level(AV_LOG_QUIET);

	return 0;
}

SamediffVideo *samediff_video_alloc(void)
{
	return calloc(1, sizeof(SamediffVideo));
}

int samediff_video_open(SamediffVideo *video,
                        const char *const path,
                        const samediff_hw_mode hw_mode)
{
	int ret                       = 0;
	AVCodec *codec                = NULL;
	const AVCodecHWConfig *config = NULL;
	video->format_context         = avformat_alloc_context();

	if ((ret = avformat_open_input(&video->format_context, path, NULL, NULL)) != 0) {
		return ret;
	}

	return -1;

	if ((ret = avformat_find_stream_info(video->format_context, NULL)) < 0) {
		return ret;
	}

	if ((ret = av_find_best_stream(video->format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0)) <
	    0) {
		return ret;
	}

	video->stream_index = ret;

	for (int stream_index = 0; stream_index < (int)video->format_context->nb_streams;
	     stream_index++) {
		if (stream_index != video->stream_index) {
			video->format_context->streams[stream_index]->discard = AVDISCARD_ALL;
		}
	}

	video->codec_context               = avcodec_alloc_context3(codec);
	video->codec_context->thread_count = 1;

	if ((ret = avcodec_parameters_to_context(
	         video->codec_context, video->format_context->streams[video->stream_index]->codecpar)) <
	    0) {
		return ret;
	}

	if (hw_mode != SAMEDIFF_HW_MODE_OFF) {
		for (int i = 0;; i++) {
			config = avcodec_get_hw_config(codec, i);

			if (config == NULL || config->device_type == AV_HWDEVICE_TYPE_NONE) {
				break;
			}

			if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX) {
				if ((ret = av_hwdevice_ctx_create(&video->codec_context->hw_device_ctx,
				                                  config->device_type,
				                                  NULL,
				                                  NULL,
				                                  0)) < 0) {
					debug("Tried and failed to setup %s hardware decoder for %s",
					      av_hwdevice_get_type_name(config->device_type),
					      codec->name);

					continue;
				}

				video->hw_pix_format = config->pix_fmt;
				video->hw_device     = config->device_type;

				debug("Enabled %s hardware decoder for %s",
				      av_hwdevice_get_type_name(config->device_type),
				      codec->name);

				break;
			}
		}
	}

	if (hw_mode == SAMEDIFF_HW_MODE_FORCE && video->hw_device == AV_HWDEVICE_TYPE_NONE) {
		warning("Cannot force hardware acceleration for %s", codec->name);

		return -EXIT_FAILURE;
	}

	if ((ret = avcodec_open2(video->codec_context, codec, NULL)) < 0) {
		return ret;
	}

	if (video->format_context->duration == AV_NOPTS_VALUE) {
		video->type = MEDIA_TYPE_IMAGE;
	} else {
		video->type = MEDIA_TYPE_VIDEO;
	}

	return 0;
}

SamediffVideoInfo *samediff_video_get_info(SamediffVideo *video)
{
	if (video->info == NULL) {
		video->info         = calloc(1, sizeof(SamediffVideoInfo));
		video->info->format = (char *)video->format_context->iformat->long_name;
		video->info->codec  = (char *)video->codec_context->codec->name;
		video->info->width  = video->codec_context->width;
		video->info->height = video->codec_context->height;
		video->info->fps =
		    av_q2d(video->format_context->streams[video->stream_index]->avg_frame_rate);

		if (video->info->fps != video->info->fps) {
			video->info->fps = 0;
		}
	}

	return video->info;
}

int samediff_video_read_frame(SamediffVideo *video, SamediffVideoFrame *frame, double position)
{
	const AVRational time_base = video->format_context->streams[video->stream_index]->time_base;
	int64_t ts                 = position * time_base.den / time_base.num;
	int ret                    = 0;
	AVPacket packet;

	while (true) {
		ret = av_read_frame(video->format_context, &packet);

		if (ret < 0) {
			av_packet_unref(&packet);

			if (ret != AVERROR_EOF) {
				warning("");
			}

			avcodec_send_packet(video->codec_context, NULL);

			break;
		}

		if (packet.stream_index != video->stream_index) {
			av_packet_unref(&packet);

			continue;
		}

		ret = avcodec_send_packet(video->codec_context, &packet);

		if (ret < 0 && ret != AVERROR(EAGAIN) && ret != AVERROR_INVALIDDATA) {
			av_packet_unref(&packet);

			if (ret != AVERROR_EOF) {
				warning(av_err2str(ret));
			}

			break;
		}

		ret = avcodec_receive_frame(video->codec_context, frame->frame);

		av_packet_unref(&packet);

		if (ret == AVERROR(EAGAIN) || ret == AVERROR_INVALIDDATA) {
			continue;
		} else if (ret < 0) {
			if (ret != AVERROR_EOF) {
				warning("");
			}

			break;
		}

		if (position > 0 && frame->frame->best_effort_timestamp < ts) {
			continue;
		}

		// Frame was decoded in hardware, we need to copy the data to the CPU.
		if (frame->frame->hw_frames_ctx != NULL) {
			if ((ret = av_hwframe_transfer_data(frame->sw_frame, frame->frame, 0)) < 0) {
				break;
			}

			frame->frame_to_fingerprint = frame->sw_frame;
		} else {
			frame->frame_to_fingerprint = frame->frame;
		}

		break;
	}

	return ret;
}

void samediff_video_free(SamediffVideo **video)
{
	if (video == NULL || *video == NULL) {
		return;
	}

	if ((*video)->grey_frame != NULL) {
		av_freep(&(*video)->grey_frame->data);
		av_frame_free(&(*video)->grey_frame);
	}

	if ((*video)->codec_context != NULL) {
		av_buffer_unref(&(*video)->codec_context->hw_device_ctx);
		avcodec_free_context(&(*video)->codec_context);
	}

	sws_freeContext((*video)->sws_context);
	avformat_close_input(&(*video)->format_context);

	freep(&(*video)->info);
	freep(video);
}

SamediffVideoFingerprint *samediff_video_fingerprint_alloc(void)
{
	return calloc(1, sizeof(SamediffVideoFingerprint));
}

int samediff_video_fingerprint_frame(SamediffVideo *video, SamediffVideoFingerprint *fingerprint)
{
	debug("Calling %s()", __func__);

	return samediff_video_fingerprint_frames(video, fingerprint, 1, 0);
}

int samediff_video_fingerprint_frames(SamediffVideo *video,
                                      SamediffVideoFingerprint *fingerprint,
                                      const int num_frames,
                                      const double interval)
{
	debug("Calling %s()", __func__);

	int ret                    = 0;
	int num_decoded            = 0;
	SamediffVideoFrame *frame  = samediff_video_frame_alloc();
	double position            = 0;
	int64_t ts                 = 0;
	const AVRational time_base = video->format_context->streams[video->stream_index]->time_base;

	if (frame == NULL) {
		return AVERROR(ENOMEM);
	}

	for (; num_frames <= 0 || num_decoded < num_frames; num_decoded++) {
		if (interval > 0) {
			ts = position * time_base.den / time_base.num;

			if ((ret = avformat_seek_file(
			         video->format_context, video->stream_index, INT64_MIN, ts, ts, 0)) < 0) {
				break;
			}
		}

		if ((ret = samediff_video_read_frame(video, frame, position)) != 0) {
			if (ret != AVERROR_EOF) {
				warning("");
			}

			break;
		}

#ifdef DEBUG
		if (num_decoded > 0 && num_decoded % 1000 == 0) {
			debug("Decoded %d frames", num_decoded);
		}
#endif

		if (interval > 0) {
			position += interval;
		}

		if (num_decoded > 0) {
			fingerprint->next = samediff_video_fingerprint_alloc();
			fingerprint       = fingerprint->next;
		}

		if ((ret = compute_frame_fingerprint(video,
		                                     frame,
		                                     FRAME_FINGERPRINT_WIDTH,
		                                     FRAME_FINGERPRINT_HEIGHT,
		                                     fingerprint->fingerprint)) != 0) {
			samediff_video_fingerprint_free(&(fingerprint));

			break;
		}
	}

	samediff_video_frame_free(&frame);

	if (ret == 0 || ret == AVERROR_EOF) {
		ret = num_decoded;
	}

	debug("num decoded: %d", num_decoded);

	return ret;
}

int samediff_video_fingerprint_video(SamediffVideo *video,
                                     SamediffVideoFingerprint *fingerprint,
                                     const double interval)
{
	debug("Calling %s()", __func__);

	return samediff_video_fingerprint_frames(video, fingerprint, 0, interval);
}

double samediff_video_fingerprint_compare(const SamediffVideoFingerprint *fingerprint_1,
                                          const SamediffVideoFingerprint *fingerprint_2)
{
	int total_bits = 0;
	int diff_bits  = 0;
	int byte       = 0;
	uint8_t xor    = 0;

	while (fingerprint_1 != NULL || fingerprint_2 != NULL) {
		total_bits += FRAME_FINGERPRINT_BUFFER_SIZE_BITS;

		if (fingerprint_1 == NULL) {
			diff_bits += FRAME_FINGERPRINT_BUFFER_SIZE_BITS;
			fingerprint_2 = fingerprint_2->next;

			continue;
		}

		if (fingerprint_2 == NULL) {
			diff_bits += FRAME_FINGERPRINT_BUFFER_SIZE_BITS;
			fingerprint_1 = fingerprint_1->next;

			continue;
		}

		for (byte = 0; byte < FRAME_FINGERPRINT_BUFFER_SIZE_BYTES; byte++) {
			xor = fingerprint_1->fingerprint[byte] ^ fingerprint_2->fingerprint[byte];

			while (xor != 0) {
				diff_bits += xor&1;
				xor >>= 1;
			}
		}

		fingerprint_1 = fingerprint_1->next;
		fingerprint_2 = fingerprint_2->next;
	}

	if (total_bits == 0) {
		return 0;
	}

	return 100 - ((double)diff_bits / total_bits * 100);
}

void samediff_video_fingerprint_free(SamediffVideoFingerprint **fingerprint)
{
	if (fingerprint == NULL || *fingerprint == NULL) {
		return;
	}

	SamediffVideoFingerprint *head = *fingerprint;
	SamediffVideoFingerprint *node = NULL;

	while (head != NULL) {
		node = head;
		head = head->next;

		free(node);
	}

	fingerprint = NULL;
}
