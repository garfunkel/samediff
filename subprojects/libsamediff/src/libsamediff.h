/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <err.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

#define LIBSAMEDIFF_INSIDE
#include "libsamediff-version.h"
#undef LIBSAMEDIFF_INSIDE

typedef enum {
	SAMEDIFF_HW_MODE_OFF,
	SAMEDIFF_HW_MODE_ATTEMPT,
	SAMEDIFF_HW_MODE_FORCE
} samediff_hw_mode;

typedef struct SamediffVideo SamediffVideo;
typedef struct SamediffVideoFrame SamediffVideoFrame;
typedef struct SamediffVideoFingerprint SamediffVideoFingerprint;

typedef struct {
	char *format;
	char *codec;
	int width;
	int height;
	float fps;
} SamediffVideoInfo;

/**
 * Initialises the samediff library.
 */
int samediff_init(void);

/**
 * Allocates a new video.
 */
SamediffVideo *samediff_video_alloc(void);

/**
 * Opens a video file, setting up hardware decoding (if available).
 */
int samediff_video_open(SamediffVideo *video,
                        const char *const path,
                        const samediff_hw_mode hw_mode);

/**
 * Returns a struct containing information about an opened video.
 * The returned object is video->info will be freed by samediff_video_free().
 */
SamediffVideoInfo *samediff_video_get_info(SamediffVideo *video);

/**
 * Reads a frame of video from the opened video.
 */
int samediff_video_read_frame(SamediffVideo *video,
                              SamediffVideoFrame *frame,
                              const double position);

/**
 * Frees a video and set it to NULL.
 */
void samediff_video_free(SamediffVideo **video);

/**
 * Allocates a new video frame.
 */
SamediffVideoFrame *samediff_video_frame_alloc(void);

/**
 * Frees a video frame and set it to NULL.
 */
void samediff_video_frame_free(SamediffVideoFrame **frame);

/**
 * Allocates a new fingerprint.
 */
SamediffVideoFingerprint *samediff_video_fingerprint_alloc(void);

/**
 * Fingerprints a given frame.
 */
int samediff_video_fingerprint_frame(SamediffVideo *frame, SamediffVideoFingerprint *fingerprint);

/**
 * Fingerprints a number of sequential frames.
 */
int samediff_video_fingerprint_frames(SamediffVideo *video,
                                      SamediffVideoFingerprint *fingerprint,
                                      const int num_frames,
                                      const double interval);

/**
 * Fingerprints each and every frame in a video file.
 */
int samediff_video_fingerprint_video(SamediffVideo *video,
                                     SamediffVideoFingerprint *fingerprint,
                                     const double interval);

/**
 * Compare fingerprints, returning a float value representing the percentage of similarity.
 */
double samediff_video_fingerprint_compare(const SamediffVideoFingerprint *fingerprint_1,
                                          const SamediffVideoFingerprint *fingerprint_2);

/**
 * Frees a fingerprint and set it to NULL.
 */
void samediff_video_fingerprint_free(SamediffVideoFingerprint **fingerprint);

__END_DECLS
