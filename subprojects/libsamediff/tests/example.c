/**
 * MIT License
 *
 * Copyright (c) 2019 Simon Allen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "libsamediff.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	if (argc != 3) {
		exit(EXIT_FAILURE);
	}

	if (samediff_init() != 0) {
		exit(EXIT_FAILURE);
	}

	SamediffVideo *video_1 = samediff_video_alloc();

	if (video_1 == NULL) {
		exit(EXIT_FAILURE);
	}

	SamediffVideo *video_2 = samediff_video_alloc();

	if (video_2 == NULL) {
		exit(EXIT_FAILURE);
	}

	if (samediff_video_open(video_1, argv[1], SAMEDIFF_HW_MODE_ATTEMPT) != 0) {
		exit(EXIT_FAILURE);
	}

	if (samediff_video_open(video_2, argv[2], SAMEDIFF_HW_MODE_ATTEMPT) != 0) {
		exit(EXIT_FAILURE);
	}

	SamediffVideoFingerprint *fingerprint_1 = samediff_video_fingerprint_alloc();

	if (fingerprint_1 == NULL) {
		exit(EXIT_FAILURE);
	}

	SamediffVideoFingerprint *fingerprint_2 = samediff_video_fingerprint_alloc();

	if (fingerprint_2 == NULL) {
		exit(EXIT_FAILURE);
	}

	if (samediff_video_fingerprint_video(video_1, fingerprint_1, 0.1) < 0) {
		exit(EXIT_FAILURE);
	}

	if (samediff_video_fingerprint_video(video_2, fingerprint_2, 0.1) < 0) {
		exit(EXIT_FAILURE);
	}

	double similarity = samediff_video_fingerprint_compare(fingerprint_1, fingerprint_2);

	printf("Similarity: %f%%\n", similarity);

	samediff_video_fingerprint_free(&fingerprint_1);
	samediff_video_fingerprint_free(&fingerprint_2);
	samediff_video_free(&video_1);
	samediff_video_free(&video_2);

	return 0;
}
